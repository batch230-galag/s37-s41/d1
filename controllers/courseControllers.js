// dependencies
const mongoose = require("mongoose");
const Course = require("../models/course")

// Function for adding A course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course
module.exports.addCourse = (reqBody) => {
    
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })

    return newCourse.save().then((newCourse, error) => {

        if(error) {
            return "error";
        } else {
            return newCourse
        }
    })
}

// GET all course
module.exports.getAllCourse = () => {
    return Course.find({}).then(result => {
        return result;
    })
}

// GET all Active Courses
module.exports.getActiveCourses = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    })
}

// GET Specific Course
module.exports.getCourse = (courseId) => {
    return Course.findById(courseId).then(result => {
        return result;
    })
}

// Updating a Course
module.exports.updateCourse = (courseId, newData) => {
    if(newData.isAdmin == true) {

        return Course.findByIdAndUpdate(courseId, 
            {
                name: newData.course.name,
                description: newData.course.description,
                price: newData.course.price
            }
        ).then((result, error) => {
            if(error) {
                return false
            } else {
                return result
            }
        })

    } else {
        let message = Promise.resolve('User must be ADMIN to access this functionality');
        return message.then((value) => {return value});
    }

}

// s39 Activity
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course
module.exports.addCourseForAdmins = (reqBody, checkAdmin) => {
    if(checkAdmin.isAdmin == true) {

        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price,
            slots: reqBody.slots
        })
        
        return newCourse.save()
        .then((newCourse, error) => {
        
            if(error) {
                return "error";
            } else {
                return newCourse
            }
        })
    } else {
        let message = Promise.resolve('User must be ADMIN to access this functionality');
        return message.then((value) => {return value});
    }

}


// s40 Activity
module.exports.courseArchive = (courseId, newData) => {
    if(newData.isAdmin == true) {

        return Course.findByIdAndUpdate(courseId, 
            {
                isActive: newData.course.isActive,
                name: newData.course.name,
                description: newData.course.description,
                price: newData.course.price
            }
        ).then((result, error) => {
            if(error) {
                return false
            } else {
                return true
            }
        })

    } else {
        let message = Promise.resolve('User must be ADMIN to access this functionality');
        return message.then((value) => {return value});
    }

}