const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/course");



module.exports.registerUser = (reqBody) => {
    return User.find({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName
    })
    .then(result =>
        {
            if(result.length > 0) {
                return ("Duplicate Information")
            } else {
                let newUser = new User({
                    firstName: reqBody.firstName,
                    lastName: reqBody.lastName,
                    email: reqBody.email,
                    mobileNo: reqBody.mobileNo,
                    password: bcrypt.hashSync(reqBody.password, 10) //10 - salt
                })
            
                return newUser.save()
                .then((result,error) => {
                    if(error) {
                        return false;
                    } else {
                        return newUser;
                    }
                })
            }
        })
        
} 


/* s38 */

// function checkEmailExist{}
module.exports.checkEmailExist = (reqBody) => {
    return User.find({email: reqBody.email})
    .then(result =>
        {
            if(result.length > 0) {
                return ("Duplicate Email")
            } else {
                return false
            }
        })
}


module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email})
    .then((result) => {
        
        console.log(User);

        if (result == null) {
            return false;
        } else {
            // compareSync is a bcrypt function to compare unhashed password to hashed password
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
            // true of false
            
            if(isPasswordCorrect) {
                // Let's give the user a token to a access features
                return {access: auth.createAccessToken(result)};
            } else {
                // If password does not match, else
                return false
            }
        }
    })
}


// Activity Code (s38)
module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody.id)
    .then((result) => {

        if (result == null) {
            return false;
        } else {
            result.password = null;
            return result
        }
    })
}

// s41
module.exports.getProfile2 = (request, response) => {

    // will contain the decoded token
    const userData = auth.decode(request.headers.authorization);
    console.log(userData);

    return User.findById(userData.id).then(result => {
        result.password = "*****";
        response.send(result);
    })
}

// Enroll Feature
module.exports.enroll = async(request, response) => {
    
    const userData = auth.decode(request.headers.authorization);
    
    let courseName = await Course.findById(request.body.courseId).then(result => result.name);

    let newData = {
        // User ID and Email will be retrieved from the request header (request header contains the user token)
        userId: userData.id,
        email: userData.email,
        // Course ID will be retrieved from the request body
        courseId: request.body.courseId,
        courseName: courseName
    }

    console.log(newData);

    let isUserUpdated = await User.findById(newData.userId)
    .then(user => 
    {
        user.enrollments.push({
            courseId: newData.courseId,
            courseName: newData.courseName
        })
        return user.save()
        .then(result => {
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error)
            return false;
        })
    })
    console.log(isUserUpdated);

    let isCourseUpdated = await Course.findById(newData.courseId)
    .then(course => 
    {
        course.enrollees.push({
            userId: newData.userId,
            email: newData.email
        })

        // Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a response that a user is not allowed to enroll due to no available slots
		// [3] Else if the slot is negative value, it should make the slot value to zero
        // course.slots = course.slots - 1
        if (course.slots <= 0) {
            return console.log("No more slots lefts")
        } else {
            course.slots -= 1;
            return course.save()
            .then(result => {
                console.log(result)
                return true
            })
            .catch(error => {
                console.log(error)
                return false
            })
        }

    })
    console.log(isCourseUpdated);

    // Ternary Operator
    (isUserUpdated == true && isCourseUpdated == true) ? response.send(true) : response.send(false)

}