/*
    npm init -y
    npm install express mongoose cors
*/

// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")


// to create an express server/application
const app = express();

// Middlewares - allows to bridge our backend application (server) to our frontend
// to allow cross origin resource sharing 
app.use(cors());
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended: true}));



app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch230.rr5eflw.mongodb.net/courseBooking?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Galag-Mongo DB Atlas"));



// Don't forget this code below 
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000} `)
})