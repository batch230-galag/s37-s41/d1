// dependecies
const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// s39 discussion
router.post("/create", (request, response) => {
    courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))
});

// Get all courses
router.get("/all", (request, response) => {
    courseControllers.getAllCourse().then(resultFromController => {
        response.send(resultFromController)
    })
})

// Get Active courses
router.get("/active", (request, response) => {
    courseControllers.getActiveCourses().then(resultFromController => {
        response.send(resultFromController)
    })
})

// Get Specific courses
router.get("/:courseId", (request, response) => {
    courseControllers.getCourse(request.params.courseId).then(resultFromController => {
        response.send(resultFromController)
    })
})

// Update specific courses
router.patch("/:courseId/update", auth.verify, (request, response) => {

    const newData = {
        course: request.body,
        // request.headers.authorization -> contains JWT
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    // newData.course.name
    // newData.request.body.name
    courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
        response.send(resultFromController)
    })
})

// Activity (s39)
// 1. Refactor the "course" route to implement user authentication for the admin when creating a course.
router.post("/create/admin", auth.verify, (request, response) => {

    const checkAdmin = {
        course: request.body,
        // request.headers.authorization -> contains JWT
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    courseControllers.addCourseForAdmins(request.body, checkAdmin).then(resultFromController => {
        response.send(resultFromController)
    })
})

// s40 Activity
router.patch("/:courseId/archive", auth.verify, (request, response) => {

    const newData = {
        course: request.body,
        // request.headers.authorization -> contains JWT
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    // newData.course.name
    // newData.request.body.name
    courseControllers.courseArchive(request.params.courseId, newData).then(resultFromController => {
        response.send(resultFromController)
    })
})




// Don't forget this code below 
module.exports = router;
